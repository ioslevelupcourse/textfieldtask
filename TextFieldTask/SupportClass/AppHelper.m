//
//  AppHelper.m
//  TextFieldTask
//
//  Created by Admin on 16.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "AppHelper.h"

@implementation AppHelper

#pragma mark - Constants Privat
static NSString *const LUDateFormat = @"dd-MMM-yyyy";

#pragma mark - Public
+ (void)subscribeToKeyboardNotificationsObject:(id)object {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:object
                           selector:@selector(keyboardWillShow:)
                               name:UIKeyboardWillShowNotification
                             object:nil];
    
    [notificationCenter addObserver:object
                           selector:@selector(keyboarWillHide:)
                               name:UIKeyboardWillChangeFrameNotification
                             object:nil];
    
    [notificationCenter addObserver:object
                           selector:@selector(keyboarWillHide:)
                               name:UIKeyboardWillHideNotification
                             object:nil];
}

+ (void)unsubscribeFromKeyboardNotificationsObject:(id)object {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:object
                                  name:UIKeyboardWillShowNotification
                                object:nil];
    
    [notificationCenter removeObserver:object
                                  name:UIKeyboardWillChangeFrameNotification
                                object:nil];
    
    [notificationCenter removeObserver:object
                                  name:UIKeyboardWillHideNotification
                                object:nil];
}

+ (BOOL)isValidSymbolsForName:(NSString *)name {
    
    if (!name.length) return NO;
    
    NSMutableCharacterSet *allowedSet = [NSMutableCharacterSet characterSetWithCharactersInString:@" "];
    
    [allowedSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
    
    NSCharacterSet *forbiddenSet = [allowedSet invertedSet];
    
    NSRange range = [name rangeOfCharacterFromSet:forbiddenSet];
    
    NSLog(@"%lu", (unsigned long)name.length);
    
    if (range.location != NSNotFound || name.length > 24) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)isValidSymbolsForAge:(NSString *)string {
    
    if (!string.length) return NO;
    
    NSCharacterSet *characters = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    NSRange range = [string rangeOfCharacterFromSet:characters];
    
    if (range.location != NSNotFound || string.length > 3) {
        return NO;
    }
    
    NSInteger age = [string integerValue];
    
    if (age < 0 || age > 110) {
        return NO;
    }
    
    return YES;
}


+ (BOOL)stringIsEmpty:(NSString *)string {
    
    if ((NSNull *) string == [NSNull null]) {
        return YES;
    }
    
    if (string == nil) {
        
        return YES;
        
    } else if ([string length] == 0) {
        
        return YES;
        
    } else {
        
        string = [string stringByTrimmingCharactersInSet:
                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ([string length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

+ (NSArray *)countryNames {
    
    NSMutableArray *accumulator = [NSMutableArray new];
    
    NSArray *arrayISOCountryCodes = [NSLocale ISOCountryCodes];
    
    for (id countryCode in arrayISOCountryCodes) {
        
        NSLocale *locale = [NSLocale currentLocale];
        
        NSString *item = [locale displayNameForKey:NSLocaleCountryCode
                                             value:countryCode];
        
        [accumulator addObject:item];
    
    }
    
    return [accumulator copy];
}

+ (NSString *)stringFromDate:(NSDate *)dataBirthday {
    
    return [[AppHelper dateFormatterWithFormat:LUDateFormat] stringFromDate:dataBirthday];
}

+ (NSDate *)dateFromString:(NSString *)string {

    return [[AppHelper dateFormatterWithFormat:LUDateFormat] dateFromString:string];
}

#pragma mark - Privat
+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)string {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:LUDateFormat];
    
    return dateFormat;
}

@end
