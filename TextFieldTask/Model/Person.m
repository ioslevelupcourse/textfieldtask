//
//  Person.m
//  TextFieldTask
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "Person.h"

@implementation Person

- (NSString *)allPropertyToString {
    
    NSString* string = [NSString stringWithFormat:
                        @"FirstName:%@\n LastName:%@\n Sex:%@\n Contry:%@\n Age:%@",
                        self.firstName,
                        self.lastName,
                        self.sex ? @"Famale" : @"Male",
                        self.country,
                        @(self.age).stringValue];
    
    return string;
}

@end
