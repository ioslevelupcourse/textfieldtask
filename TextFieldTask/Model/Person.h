//
//  Person.h
//  TextFieldTask
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property(nonatomic, copy) NSString *firstName;
@property(nonatomic, copy) NSString *lastName;
@property(nonatomic, copy) NSString *country;

@property(nonatomic, assign) BOOL sex;

@property(nonatomic, assign) NSInteger age;

- (NSString *)allPropertyToString;

@end
