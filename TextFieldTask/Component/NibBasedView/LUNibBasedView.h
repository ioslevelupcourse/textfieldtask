//
//  ActionSheetPickerView.h
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LUNibBasedView : UIView

#pragma mark - Constants
extern CGFloat const PickerPickerDataHeight;

#pragma mark - Public

@property (weak, nonatomic) IBOutlet UIView *pickerContainerView;

@property (nonatomic, strong) id selected;
@property (nonatomic, copy) void(^selectCallBack)(id);

- (void)presentPicker;
- (void)dismissPicker;

@end
