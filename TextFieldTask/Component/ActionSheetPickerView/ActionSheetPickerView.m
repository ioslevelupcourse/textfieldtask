//
//  ActionSheetPickerView.m
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "ActionSheetPickerView.h"

@interface ActionSheetPickerView ()

@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) id startSelectItem;

@end

@implementation ActionSheetPickerView

#pragma mark - Initialize class
 //Why does not call?
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    [self xibSetup2];
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    [self xibSetup2];
    
    return self;
}


- (void)initializeProperties2 {
}

- (UIView *)loadViewFromNib2 {
    
    NSBundle *bandle = [NSBundle bundleForClass:[self class]];
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class])
                                bundle:bandle];
    
    UIView *view = [[nib instantiateWithOwner:self
                                      options:nil] firstObject];
    
    return view;
}

- (void)prepareForInterfaceBuilder {
    [self xibSetup2];
}

- (void)xibSetup2 {
    
    UIView *view = [self loadViewFromNib2];
    
    view.frame = self.pickerContainerView.bounds;
    
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    
    [self.pickerContainerView addSubview:view];
    
    [self initializeProperties2];
}

#pragma mark - Initialize class
+ (instancetype)stringPickerWithItems:(NSArray <NSString *> *)items
                      startSelectItem:(id)item
                       selectCallBack:(void(^)(id selected))selectRowBlock {
    
    ActionSheetPickerView *actionSheetPickerView = [ActionSheetPickerView stringPickerWithItems:items selectCallBack:selectRowBlock];
    
    NSInteger index = [items indexOfObject:item];
    
    if (index != NSNotFound) {
        
        [actionSheetPickerView.picker selectRow:index
                                    inComponent:0
                                       animated:NO];
        
        actionSheetPickerView.selected = item;
        
    } else {
        
        NSLog(@"Index not found in items");
        
        actionSheetPickerView.selected = items.firstObject;
        
    }
    
    return actionSheetPickerView;
}

+ (instancetype)stringPickerWithItems:(NSArray <NSString *> *)items
                       selectCallBack:(void(^)(id selected))selectCallBack {
    
    ActionSheetPickerView *actionSheetPicker = [ActionSheetPickerView new];
    
    actionSheetPicker.items = items;
    actionSheetPicker.selectCallBack = selectCallBack;
    
    return actionSheetPicker;
}

#pragma mark - Privat

- (void)updateSelected {
    
    NSUInteger index = [self.picker selectedRowInComponent:0];
    
    self.selected = [self.items objectAtIndex:index];
}

#pragma mark - <UIPickerViewDataSource>
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.items.count;
}


- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    
    id string = [self.items objectAtIndex:row];
    
    if (![string isKindOfClass:[NSString class]]) {
        NSLog(@"is NOT NSString class");
        string = [string stringValue];
    }
    
    return string;
}

#pragma mark - <UIPickerViewDelegate>
- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    [self updateSelected];
}

@end
