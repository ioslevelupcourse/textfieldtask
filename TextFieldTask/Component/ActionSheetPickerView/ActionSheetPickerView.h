//
//  ActionSheetPickerView.h
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LUNibBasedView.h"

@interface ActionSheetPickerView : LUNibBasedView <UIPickerViewDataSource, UIPickerViewDelegate>

+ (instancetype)stringPickerWithItems:(NSArray<NSString *> *)items
                       selectCallBack:(void(^)(id selected))selectRowBlock;

+ (instancetype)stringPickerWithItems:(NSArray <NSString *> *)items
                      startSelectItem:(id)item
                       selectCallBack:(void(^)(id selected))selectRowBlock;

@end
