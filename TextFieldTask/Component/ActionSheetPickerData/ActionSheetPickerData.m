//
//  ActionSheetPickerData.m
//  TextFieldTask
//
//  Created by Admin on 14.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "ActionSheetPickerData.h"

@interface ActionSheetPickerData ()

@property (weak, nonatomic) IBOutlet UIDatePicker *pickerData;

@property (nonatomic, strong) id startSelectItem;

@end

@implementation ActionSheetPickerData

#pragma mark - Initialize
+ (instancetype)pickerDataWithMode:(UIDatePickerMode)datePickerMode
                              from:(NSDate *)minimumDate
                                to:(NSDate *)maximumDate
                          interval:(NSInteger)minuteInterval
                   startSelectItem:(NSDate *)item
                    selectCallBack:(void(^)(id selected))selectCallBack {
    
    ActionSheetPickerData *actionSheetPickerData = [ActionSheetPickerData new];
    
    [actionSheetPickerData createPikerDataInPickerContainerViewAndAddTarget];

    actionSheetPickerData.pickerData.datePickerMode = datePickerMode;
    actionSheetPickerData.pickerData.minimumDate = minimumDate;
    actionSheetPickerData.pickerData.maximumDate = maximumDate;
    actionSheetPickerData.pickerData.minuteInterval = minuteInterval;
    
    if (item) {
        actionSheetPickerData.pickerData.date = item;
        actionSheetPickerData.selected = item;
    } else {
        NSDate *currentDate = [NSDate date];
        actionSheetPickerData.selected = currentDate;
    }
    
    actionSheetPickerData.selectCallBack = selectCallBack;
    
    return actionSheetPickerData;
}

+ (instancetype)pickerDataWithStartSelectItem:(NSDate *)item
                               selectCallBack:(void(^)(id selected))selectCallBack {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *currentDate = [NSDate date];
    
    NSDateComponents *component = [[NSDateComponents alloc] init];
    
    [component setYear:-115];
    
    NSDate *minimumDate = [calendar dateByAddingComponents:component
                                                    toDate:currentDate
                                                   options:0];
    
    ActionSheetPickerData *actionSheetPickerData =
    [ActionSheetPickerData pickerDataWithMode:UIDatePickerModeDate
                                         from:minimumDate
                                           to:currentDate
                                     interval:0
                              startSelectItem:item
                               selectCallBack:selectCallBack];
    
    return actionSheetPickerData;
}

#pragma mark - Life
- (void)dealloc {
    
    [self.pickerData removeTarget:nil
                           action:NULL
                 forControlEvents:UIControlEventAllEvents];
    
}

#pragma mark - <Action>
- (void)updateSelected:(id)sender {
    self.selected = self.pickerData.date;
}

#pragma mark - Privat
- (void)createPikerDataInPickerContainerViewAndAddTarget {

    UIDatePicker *pickerData = [[UIDatePicker alloc] initWithFrame:self.pickerContainerView.bounds];
    
    pickerData.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    
    [self.pickerContainerView addSubview:pickerData];

    [pickerData addTarget:self
                   action:@selector(updateSelected:)
         forControlEvents:UIControlEventValueChanged];
    
    self.pickerData = pickerData;
    
}

@end
