//
//  SighUpViewController.m
//  TextFieldTask
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "SighUpViewController.h"
#import "ActionSheetPickerView.h"

#import "Person.h"

#import "AppHelper.h"

static const CGFloat heightTabBar = 49.f;
static const CGFloat clearanceAboveKeyboard = 5.f;

@interface SighUpViewController() <AppKeyboardNotifications>

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;

@property (weak, nonatomic) IBOutlet UISwitch *sexSwitch;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (nonatomic, strong) ActionSheetPickerView *pickerView;

@property (nonatomic, strong) Person    *person;
@property (nonatomic, assign) BOOL      isKeyboardOpen;
@property (nonatomic, assign) double    defaultValueBottomConstraint;

@end

@implementation SighUpViewController

#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.person = [Person new];
    self.isKeyboardOpen = NO;
    self.defaultValueBottomConstraint = self.bottomConstraint.constant;
    
    [self.firstNameTextField becomeFirstResponder];
    
    [self.sexSwitch setOn:NO animated:NO];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AppHelper subscribeToKeyboardNotificationsObject:self];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [AppHelper unsubscribeFromKeyboardNotificationsObject:self];
    
}

#pragma mark - Actions
- (IBAction)showButtonDidClick:(UIButton*)sender {
    
    NSString* message;
    
    if ([AppHelper isValidSymbolsForName: self.firstNameTextField.text]  &&
        [AppHelper isValidSymbolsForName: self.lastNameTextField.text]   &&
        [AppHelper isValidSymbolsForName: self.countryTextField.text]    &&
        [AppHelper isValidSymbolsForAge: self.ageTextField.text]) {
        
        self.person.firstName   = self.firstNameTextField.text;
        self.person.lastName    = self.lastNameTextField.text;
        self.person.sex         = self.sexSwitch.on;
        self.person.country     = self.countryTextField.text;
        self.person.age         = [self.ageTextField.text intValue];
        
        message = [self.person allPropertyToString];
        
    } else {
        
        if (![AppHelper isValidSymbolsForAge: self.ageTextField.text]) {
            message = msgCheckEnteredAgeAgain;
        } else {
            message = msgCheckEnteredDataAgain;
        }
        
    }
    
    [self presentAlertDataImputWithMessage:message
                                controller:self];
    
}

- (IBAction)backgraundViewDidClick:(UIControl*)sender {
    
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    [self.countryTextField resignFirstResponder];
    [self.ageTextField resignFirstResponder];
    
}

#pragma mark - Private
- (void)keyboardWillShow:(NSNotification *)notificion {
    
    if (!self.isKeyboardOpen) {

        CGFloat heightKeyboard = [self takeHeightKeyboardOfUserInfo:notificion.userInfo];
        
        CGFloat height = heightKeyboard - heightTabBar + clearanceAboveKeyboard;
        
        [self animationMovement:height];
        
        self.isKeyboardOpen = YES;
        
    }
    
}

- (void)keyboarWillHide:(NSNotification *)notificion {
    
    if (self.isKeyboardOpen) {
        
        [self animationMovement:self.defaultValueBottomConstraint];
        self.isKeyboardOpen = NO;
        
    }
    
}

- (void)animationMovement:(CGFloat)currentConsraint {
    
    [UIView animateWithDuration:0.9
                     animations:^{
                         
                         self.bottomConstraint.constant = currentConsraint;
                         [self.view layoutIfNeeded];
                         
                     }];
    
}

#pragma mark - <UITextFieldDelegate>
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    BOOL result = YES;
    NSString* message;
    
    if ([textField isEqual:self.ageTextField]) {
        
        if (self.ageTextField.text == NULL || self.ageTextField.text.length == 0) {
            
            message = msgEnterAge;
            result = NO;
            
        } else if (![AppHelper isValidSymbolsForAge:self.ageTextField.text]) {
            
            message = msgPostAnIncorrectEntry;
            result = NO;
        }
        
    } else {
        
        if (textField.text == NULL || textField.text.length == 0) {
            
            message = msgEmptyInput;
            result = NO;
            
        } else if (textField.text.length <= 2) {
            
            message = msgMoreThanTwoCharacters;
            result = NO;
            
        } else if (textField.text.length > 40) {
            
            message = msgNoMoreThanCharacters;
            result = NO;
            
        } else if (![AppHelper isValidSymbolsForName: textField.text]) {
            
            message = msgPostAnIncorrectEntry;
            result = NO;
        }
    }
    
    if (result) {
        
        if ([textField isEqual:self.firstNameTextField]) {
            
            [self.lastNameTextField becomeFirstResponder];
            
        } else if ([textField isEqual:self.lastNameTextField]) {
            
            [self.countryTextField becomeFirstResponder];
            
        } else if ([textField isEqual:self.countryTextField]) {
            
            [self.ageTextField becomeFirstResponder];
        }
        
    } else {
        
        [self presentAlertDataImputWithMessage:message
                                    controller:self];
        
    }
    
    return result;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range
                                                                  withString:string];
    if (newString.length == 0) return YES;
    
    if (textField == self.ageTextField) {
        return [AppHelper isValidSymbolsForAge:newString];
    } else {
        return [AppHelper isValidSymbolsForName:newString];
    }
    
    return YES;
}

@end
