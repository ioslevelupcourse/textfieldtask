//
//  SighUpViewController.h
//  TextFieldTask
//
//  Created by Admin on 10.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface SighUpViewController : BaseViewController <UITextFieldDelegate>

@end

