//
//  BaseViewController.h
//  TextFieldTask
//
//  Created by Admin on 25.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

#pragma mark - Constants Public
extern NSString *const MyThingNotificationKey;
extern NSString *const msgEnterAge;
extern NSString *const msgCheckEnteredAgeAgain;
extern NSString *const msgCheckEnteredDataAgain;
extern NSString *const msgPostAnIncorrectEntry;
extern NSString *const msgEmptyInput;
extern NSString *const msgMoreThanTwoCharacters;
extern NSString *const msgNoMoreThanCharacters;

#pragma mark - Public
- (CGFloat)takeHeightKeyboardOfUserInfo:(NSDictionary*)dictionary;

- (void)presentAlertDataImputWithMessage:(NSString *)message
                              controller:(UIViewController *)controller;

- (void)presentAlert:(NSString *)title
             message:(NSString *)message
          controller:(UIViewController *)controller;


@end
