//
//  BaseViewController.m
//  TextFieldTask
//
//  Created by Admin on 25.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

NSString *const msgEnterAge = @"Enter age";
NSString *const msgCheckEnteredAgeAgain = @"Check the entered age again";
NSString *const msgCheckEnteredDataAgain = @"Check the entered data again";
NSString *const msgPostAnIncorrectEntry = @"Post an incorrect entry";
NSString *const msgEmptyInput = @"Empty input";
NSString *const msgMoreThanTwoCharacters = @"More than two characters";
NSString *const msgNoMoreThanCharacters = @"No more than 40 characters";

#pragma mark - Public
- (CGFloat)takeHeightKeyboardOfUserInfo:(NSDictionary*)dictionary {
    
    CGRect keyboardFrame = [dictionary [UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    double height = keyboardFrame.size.height;
        
    return height;
}

- (void)presentAlertDataImputWithMessage:(NSString *)message
                              controller:(UIViewController *)controller {
    
    [self presentAlert:@"Data Input"
                    message:message
                 controller:controller];
}

- (void)presentAlert:(NSString *)title
             message:(NSString *)message
          controller:(UIViewController *)controller {
    
    UIAlertController* alertView = [UIAlertController alertControllerWithTitle: title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
    
    [alertView addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
    
    [controller presentViewController:alertView
                             animated:true
                           completion:nil];
}


@end
