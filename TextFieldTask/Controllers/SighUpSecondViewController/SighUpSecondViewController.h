//
//  SighUpSecondViewController.h
//  TextFieldTask
//
//  Created by Admin on 16.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface SighUpSecondViewController : BaseViewController <UITextFieldDelegate>

@end
